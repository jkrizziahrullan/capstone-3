import { Fragment } from "react";
import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
import { Carousel } from "react-bootstrap";


export default function Lookbook() {
  const today = new Date();
  const date =
    today.getFullYear() + "/" + (today.getMonth() + 1) + "/" + today.getDate();
  const time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

  return (
    <div id="logo">
      <div className="text-center">
        <Link to="/">
          <img src="/images/logo.png" className="img" height="45px" />
        </Link>
        <p>
          {date}&nbsp; {time}&nbsp;PH
        </p>
      </div>

      <div style={{ width: "350px", margin: "0 auto" }}>
        <Carousel fade>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/images/hot3.jpeg"
              height="485px"
            />
            <Carousel.Caption>
              <h3 style={{ color: "white" }}>
                Sacred Heart GORE-TEX Shell Jacket
              </h3>
              <p style={{ fontWeight: "bold", color: "white" }}>$1000.00</p>
            </Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/images/hot4.jpeg"
              height="485px"
            />
            <Carousel.Caption>
              <h3 style={{ color: "white" }}>Supreme/Griffin Anorak</h3>
              <p style={{ fontWeight: "bold", color: "white" }}>$1000.00</p>
            </Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/images/hot6.jpeg"
              height="485px"
            />
            <Carousel.Caption>
              <h3 style={{ color: "white" }}>GORE-TEX PACLITE Jacket</h3>
              <p style={{ fontWeight: "bold", color: "white" }}>$1000.00</p>
            </Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/images/hot7.jpeg"
              height="485px"
            />
            <Carousel.Caption>
              <h3 style={{ color: "white" }}>Gummo Football Top</h3>
              <p style={{ fontWeight: "bold", color: "white" }}>$1000.00</p>
            </Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/images/hot8.jpeg"
              height="485px"
            />
            <Carousel.Caption>
              <h3 style={{ color: "white" }}>
                Supreme Vanson Leather Cordura Mesh Jacket and Shorts
              </h3>
              <p style={{ fontWeight: "bold", color: "white" }}>$1000.00</p>
            </Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/images/hot9.jpeg"
              height="485px"
            />
            <Carousel.Caption>
              <h3 style={{ color: "white" }}>
                Supreme/Mitchell & Ness Stadium Satin Varsity Jacket
              </h3>
              <p style={{ fontWeight: "bold", color: "white" }}>$1000.00</p>
            </Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/images/hot10.jpeg"
              height="485px"
            />
            <Carousel.Caption>
              <h3 style={{ color: "white" }}>
                AIO Glow-in-the-Dark Track Jacket and Pants
              </h3>
              <p style={{ fontWeight: "bold", color: "white" }}>$1000.00</p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "600px",
          margin: "0 auto",
        }}
      >
        <Link to="/" style={{ color: "#808080" }}>
          Home
        </Link>
        <Link to="/AboutUs" style={{ color: "#808080" }}>
          About Us
        </Link>
        <Link to="/Register" style={{ color: "#808080" }}>
          Register
        </Link>
        <Link to="/Login" style={{ color: "#808080" }}>
          Login
        </Link>
      </div>
    </div>
  );
}
