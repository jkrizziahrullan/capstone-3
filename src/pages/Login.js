import { useState, useEffect, useContext } from 'react';
import { Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Link, useNavigate } from 'react-router-dom';

export default function Login() {

	const today = new Date()
	const date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
	const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
	const navigate = useNavigate();
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(true);


	function authenticate(e) {

		e.preventDefault()
		fetch('https://whispering-atoll-45857.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.accessToken !== "undefined") {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)
				Swal.fire({
					title: "Login Successful",
					icon: "success",
				});
				navigate("/")
				
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}

		})


		setEmail("");
		setPassword("");
	}

	const retrieveUserDetails = (token) => {

		fetch('https://whispering-atoll-45857.herokuapp.com/api/users/details', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.type == 'Admin',
				token: token
			})
		})

	}


	useEffect(()=> {
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		<div id="logo">
			<div className="text-center">
				<Link to="/">
					<img src="images/logo.png" className="img" height="45px" />
				</Link>
				<p>
					{date}&nbsp; {time}&nbsp;PH
				</p>
			</div>
			<div style={{ width: "600px", margin: "0 auto" }}>
				<Form onSubmit={(e) => authenticate(e)}>
				<h1 className="text-center">Login</h1>
				  <Form.Group className="mb-3" controlId="userEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email"
				    	value={email}
				    	onChange={e =>setEmail(e.target.value)} 
				    	required
				    />
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="password">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value={password}
				    	onChange={e => setPassword(e.target.value)}
				    	required
				    />
				  </Form.Group>
				
				  {
				  	isActive ?
				  	<Button variant="danger" type="submit" id="submitBtn">
				  	  Submit
				  	</Button>
				  	:
				  	<Button variant="danger" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>

				  }
				</Form>
			</div>

			<div
				style={{
					display: "flex",
					justifyContent: "space-between",
					width: "600px",
					margin: "0 auto",
				}}
			>
				<Link to="/" style={{ color: "#808080" }}>
					Home
				</Link>
				<Link to="/AboutUs" style={{ color: "#808080" }}>
					About Us
				</Link>
				<Link to="/Register" style={{ color: "#808080" }}>
					Register
				</Link>
				<Link to="/lookbook" style={{ color: "#808080" }}>
					*Featured
				</Link>
			</div>
		</div>
		

		)
}
