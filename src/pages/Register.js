import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register() {
	const { user } = useContext(UserContext);

	const navigate = useNavigate();
	const today = new Date();
	const date =
		today.getFullYear() +
		"/" +
		(today.getMonth() + 1) +
		"/" +
		today.getDate();
	const time =
		today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {
		e.preventDefault();

		fetch("https://whispering-atoll-45857.herokuapp.com/api/users/register", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
				password: password1,
				type: 'Customer'
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					//Clear input fields
					setEmail("");
					setPassword1("");
					setPassword2("");

					Swal.fire({
						title: "Registration successful",
						icon: "success",
						text: "Welcome to Supreme!",
					});

					navigate("/login");
				} else {
					Swal.fire({
						title: "Something went wrong!",
						icon: "error",
						text: "Please try again.",
					});
				}
			});
	}

	useEffect(() => {
		//Validation to enable the submit button when all the input fields are populated and both passwords match
		if (
			email !== "" &&
			password1 !== "" &&
			password2 !== "" &&
			password1 === password2
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);

	return (
		<div id="logo">
			<div className="text-center">
				<Link to="/">
					<img src="images/logo.png" className="img" height="45px" />
				</Link>
				<p>
					{date}&nbsp; {time}&nbsp;PH
				</p>
			</div>
			<div style={{ width: "600px", margin: "0 auto" }}>
				<Form onSubmit={(e) => registerUser(e)}>
					<h1 className="text-center">Register</h1>

					<Form.Group className="mb-3" controlId="userEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={(e) => {
								setEmail(e.target.value);
								//console.log(e.target.value)
							}}
							required
						/>
						<Form.Text className="text-muted">
							We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>

					<Form.Group className="mb-3" controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Password"
							value={password1}
							onChange={(e) => setPassword1(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="password2">
						<Form.Label>Verify Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Password"
							value={password2}
							onChange={(e) => setPassword2(e.target.value)}
							required
						/>
					</Form.Group>

					{/*Conditionally render submit button based on the isActive state*/}
					{isActive ? (
						<Button variant="primary" type="submit" id="submitBtn">
							Submit
						</Button>
					) : (
						<Button
							variant="danger"
							type="submit"
							id="submitBtn"
							disabled
						>
							Submit
						</Button>
					)}
				</Form>
			</div>

			<div
				style={{
					display: "flex",
					justifyContent: "space-between",
					width: "600px",
					margin: "0 auto",
				}}
			>
				<Link to="/" style={{ color: "#808080" }}>
					Home
				</Link>
				<Link to="/AboutUs" style={{ color: "#808080" }}>
					About Us
				</Link>
				<Link to="/Login" style={{ color: "#808080" }}>
					Login
				</Link>
				<Link to="/lookbook" style={{ color: "#808080" }}>
					*Featured
				</Link>
			</div>
		</div>
	);
}
