import { Fragment, useState, useContext } from "react";
import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
import { Carousel, Table, Form } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from '../UserContext'


export default function NewProduct() {
  const { user } = useContext(UserContext);
  console.log(user)
  const today = new Date();
  const date =
    today.getFullYear() + "/" + (today.getMonth() + 1) + "/" + today.getDate();
  const time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [imageUrl, setimageUrl] = useState("");
  const [isActive, setIsActive] = useState(false);

  function createNewProduct(e) {
      e.preventDefault();

      fetch("https://whispering-atoll-45857.herokuapp.com/api/products/create", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${ user.token }`
        },
        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
          imageUrl: imageUrl,
          isActive: isActive
        }),
      }).then((res) => {
        Swal.fire({
          text: "Created New Product"
        })
      })
    }

  return (
    <div id="logo">
      <div className="text-center">
        <Link to="/">
          <img src="/images/logo.png" className="img" height="45px" />
        </Link>
        <p>
          {date}&nbsp; {time}&nbsp;PH
        </p>
      </div>

      <div
        style={{ width: "600px", margin: "0 auto" }}
      >
        <Form onSubmit={(e) => createNewProduct(e)}>
          <h1 className="text-center">Create New Product</h1>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Name"
              required
              onChange={(e) => setName(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Description"
              required
              onChange={(e) => setDescription(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter Product Price"
              required
              onChange={(e) => setPrice(e.target.value)}
            />
          </Form.Group>



          {/*Conditionally render submit button based on the isActive state*/}
          {isActive ? (
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button
              variant="danger"
              type="submit"
              id="submitBtn"
            >
              Create New Product
            </Button>
          )}
        </Form>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "600px",
          margin: "0 auto",
        }}
      >
      <Link to="/ProductList" style={{ color: "#808080" }}>
        *ProductList
      </Link>
      </div>
    </div>
  );
};