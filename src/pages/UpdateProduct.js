import { Fragment, useState, useContext, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
import { Carousel, Table, Form } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from '../UserContext'


export default function UpdateProduct() {
  const { user } = useContext(UserContext);
  const today = new Date();
  const date =
    today.getFullYear() + "/" + (today.getMonth() + 1) + "/" + today.getDate();
  const time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const [isActive, setIsActive] = useState(false);

  const { id } = useParams()


  // SAMPLE: const [item, setItem] = useState(initialState)
  const [product, setProduct] = useState({})

  // product = {name: qsdsadsa, id: sdsafdfasf}



  function fetchProduct(productId) {
      fetch(`https://whispering-atoll-45857.herokuapp.com/api/products/${productId}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${ user.token }`
        }
      })
      .then((res) => res.json())
      .then(data => {
        setProduct(data)
      })
    }

  function updateProduct(e, productId) {
      e.preventDefault();

      fetch(`https://whispering-atoll-45857.herokuapp.com/api/products/${productId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${ user.token }`
        },
        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
          imageUrl: imageUrl,
          isActive: isActive
        }),
      }).then((res) => {
        Swal.fire({
          text: "Updated Product"
        })
      })
    }

  // on load page execute fetchProduct
  // set product (on product change)
  // on product change (set name, price description)

  useEffect(() => {
    fetchProduct(id)
  }, [])

  useEffect(() => {
    setName(product.name)
    setDescription(product.description)
    setPrice(product.price)
    setImageUrl(product.imageUrl)
    setIsActive(product.isActive)
  }, [product])

  return (
    <div id="logo">
      <div className="text-center">
        <Link to="/">
          <img src="/images/logo.png" className="img" height="45px" />
        </Link>
        <p>
          {date}&nbsp; {time}&nbsp;PH
        </p>
      </div>

      <div
        style={{ width: "600px", margin: "0 auto" }}
      >
        <Form onSubmit={(e) => updateProduct(e, product._id )}>
          <h1 className="text-center">Update Product</h1>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Name"
              required
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Description"
              required
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter Product Price"
              required
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>imageUrl</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Image Url"
              required
              value={imageUrl}
              onChange={(e) => setImageUrl(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Availability</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product's Availability"
              required
              value={isActive}
              onChange={(e) => setIsActive(e.target.value)}
            />
          </Form.Group>


          <Button variant="primary" type="submit" id="submitBtn">
            Submit
          </Button>
        </Form>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "600px",
          margin: "0 auto",
        }}
      >
      <Link to="/ProductList" style={{ color: "#808080" }}>
        *ProductList
      </Link>
      </div>
    </div>
  );
};