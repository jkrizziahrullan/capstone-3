import { Fragment, useContext, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
import { Carousel, Table } from "react-bootstrap";
import UserContext from "../UserContext";


export default function OrderHistory() {
  const today = new Date();
  const date =
    today.getFullYear() + "/" + (today.getMonth() + 1) + "/" + today.getDate();
  const time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

  const { user, setUser } = useContext(UserContext);

  const [orders, setOrders] = useState([])
  const fetchOrdersByUser = (userId) => {
    fetch("https://whispering-atoll-45857.herokuapp.com/api/orders/", {
      headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${ user.token }`
        },
    })
      .then((result) => result.json())
      .then((data) => {
        setOrders(data);
      })
  }

  useEffect(() => {
    fetchOrdersByUser(user.id)
  }, [])

  return (
    <div id="logo">
      <div className="text-center">
        <Link to="/">
          <img src="/images/logo.png" className="img" height="45px" />
        </Link>
        <p>
          {date}&nbsp; {time}&nbsp;PH
        </p>
      </div>

      <div style={{ width: "600px", margin: "0 auto" }}>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {orders.map((order) => (
              <tr>
                <td>{order._id}</td>
                <td>{order.product.name}</td>
                <td>{order.product.description}</td>
                <td>{order.product.price}</td>
                <td>{order.status}</td>
              </tr>
            ))}
            
          </tbody>
        </Table>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "600px",
          margin: "0 auto",
        }}
      >
        {user.isAdmin && (
          <Link to="/ProductList" style={{ color: "#808080" }}>
            *ProductList
          </Link>
          )

        }

      </div>
    </div>
  );
}
