import { Fragment } from "react";
import { Link } from "react-router-dom";
import { Row, Col, Button } from 'react-bootstrap';
export default function AboutUs(){
	const today = new Date()
	const date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
	const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();


	return (
		<div id="logo">
			<div className="text-center" style={{width: "600px", margin:"0 auto"}}>
				<Link to="/">
				<img src="images/logo.png" className="img" height="45px" />
   				</Link>
				<p>{date}&nbsp; {time}&nbsp;PH</p>
			</div>

			<div style={{textAlign:"justify", width: "600px", margin:"0 auto"}}>
				<p>In April 1994, Supreme opened its doors on Lafayette Street in downtown Manhattan and became the home of New York City skate culture. At its core was a group of neighborhood kids, New York skaters, and local artist who became the store's staff, crew, and customers.</p>
				<p>Supreme grew to embody downtown culture, and play an integral part in its constant regeneration. Skaters, punks, hip-hop heads - the young counter culture at large - all gravitated toward Supreme.</p>
				<p>While it developed into a downtown institution, Supreme established itself as a brand known for its quality, style, and authenticity.</p>
				<p>Over 25 years, Supreme has expanded from its New York City origins into a global community; working with generations of artists, photographers, designers, musicians, filmakers, and writers who defied conventions and contributed to its unique identity and attitude.</p>
			</div>
			
			<div style={{display:"flex", justifyContent: "space-between", width: "600px", margin:"0 auto"}}>
				<Link to="/" style={{ color: '#808080' }}>Home</Link>
				<Link to="/Register" style={{ color: '#808080' }}>Register</Link>
				<Link to="/Login" style={{ color: '#808080' }}>Login</Link>
				<Link to="/lookbook" style={{ color: '#808080' }}>*Featured</Link>
			</div>
		</div>
		
	);
}

